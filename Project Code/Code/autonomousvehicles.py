import time
from climatecontrollerfinal import cabinTempController
from hedlightController import headLightController
from parkingAssist import parkingAssist
from hesa import hesa_controller
from mirrorAdjuster import mirrorAdjust

msg1 = "="
msg2 = "_"
msg3 = "*"
msg4 = "Driver sits in the car"
msg5 = "."
msg6 = "Set the desired temperature inside the car(~18-35): "
msg7 = "Set Your Vehile Headlight Intensity (Vehicle-1): "

print(msg4, end="")
i = 1
while i<5 :
    print (msg5, end="")
    time.sleep(1)
    i+=1
print ("\n")
mirror = input("Do you need assistance for adjusting the mirrors?(Y/N) : ")
if ((mirror == 'Y') | (mirror == 'y')):
    init_mirror_angle = [15, 35, 22]
    mirrorAdjust(init_mirror_angle)
while(True):
    threshold = input(msg6)
    t = int(threshold)
    if t>=18 and t<=35:
        break
    else:
        print("Please set the desired temperature between 18 and 35 :")
        continue
cabinTempController(t)

j=1
intensityVal = int(input(msg7))
while j < 30:
    if j==10:
        print (msg2, end="")
        hesa_controller()
    elif j==17:
        print (msg3, end="")
        headLightController(intensityVal)
    elif j==22:
        print (msg2, end="")
        hesa_controller()
    else :
        print (msg1, end="")
    time.sleep(0.5)
    j+=1

print("\nDestination Arrived.")

parkQ = input("Activate Parking assistance (Y/N): ")
if ((parkQ == 'Y') | (parkQ == 'y')):
    parkingAssist()
else:
    print('Continue driving!')


