import time
import random

c = random.randint(18,35)
msg5 = "."

def cabinTempController(threshold):
    internalTempSensor()
    tempSensor(threshold)

def internalTempSensor():
    msg2 = "Cabin climate sensor sends current temperature value to automatic climate controller --> Sending "
    msg3 = "\nController records the current cabin temperature value sent by the sensor -->  "
    
    print (msg2, end=" ")
    j=1
    while j<5 :
        print (msg5, end=" ")
        time.sleep(1)
        j+=1

    print(c)
    print(msg3, end=" ")
    print(c)
    time.sleep(2)

def heaterOn(threshold):
    print("Turning on heater")
    time.sleep(2)
    print ("Increasing the cabin temperature", end="")
    k=1
    while k<5 :
        print (msg5, end="")
        time.sleep(1)
        k+=1
    time.sleep(2)

    c = threshold
    print ("The cabin temperature is adjusted to", end=" ")
    print(c)

def acOn(threshold):
    print("Turning on A/C ")
    time.sleep(2)
    print ("Decreasing the cabin temperature ... ")
    time.sleep(5)
    c = threshold
    print ("The cabin temperature is adjusted to", end=" ")
    print (c)

def noChange():
    print("Controller maintains the temperature.")

def tempSensor(threshold):
    if c<threshold :
        heaterOn(threshold)
    elif c>threshold :
        acOn(threshold)
    elif c==threshold :
        noChange()
