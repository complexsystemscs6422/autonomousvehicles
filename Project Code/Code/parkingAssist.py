#import packages
from time import sleep
import sys
import random

#initialize
msgTech   = "Activating RADAR and Camera"
msgAssist = "Searching for Parking Location"
msgNav    = "Navigating to the location"
msgMetre  = random.randint(21,50)
PosList   = ['left', 'right']

#fuction for progressing message
def progress_gen(msg):
    i = 0
    while True:
        for j in range(0, 4):
            dots = "." * j
            sys.stdout.write("{}\r".format(msg + dots))
            i += 1
            sleep(0.5)
            sys.stdout.write("\033[K")
        yield

#main function
def parkingAssist():
    tech = progress_gen(msgTech)
    for i in range(0,100):
        next(tech)
        if i == 3:
            break
    assist = progress_gen(msgAssist)
    for i in range(0,100):
        next(assist)
        if i == 3:
            break
    sleep(0.5)
    print("Parking available at "+str(msgMetre)+" metres to your "+random.choice(PosList))
    sleep(0.5)
    print("Turned on navigation to the parking spot")    
    