import sys
import time


def progress_gen(message):
    i = 0
    while True:
        for x in range(0, 4):
            dots = "." * x
            sys.stdout.write("{}\r".format(message + dots))
            i += 1
            time.sleep(0.5)
        sys.stdout.write("\033[K")
        yield


def hesa_controller():
    print("\nSuspension Activated")
    print ("Shock absorbers engaged")
    time.sleep(1)
    print ("Hydraulic motor rotates")
    time.sleep(0.7)
    p = progress_gen("Generating charge")

    for x in range(1, 100):
        next(p)
        if x == 3:
            generator_controller()
            break

def generator_controller():
    time.sleep(1)
    print ("Charge Generated")  
    time.sleep(1)  
    print ("Battery Charged!")





