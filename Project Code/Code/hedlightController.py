import time
def headLightController(intensityVal):
    print(" \n Incoming Vehicle")
    onComingThreshold = 50
    if(intensityVal >= onComingThreshold):
        print (" Checking Treshold \n")
    else:
        print (" Checking Treshold \n")
        print ("\n----------------------------------------------------------") 
        print("                      *               *")
        print("           Vehicle-1=*                 *=Vehicle-2")
        print("                      *               *")
        print ("----------------------------------------------------------\n") 
    calculateThreshold(intensityVal,onComingThreshold)
    

def calculateThreshold(intensityVal,onComingThreshold):
    if(intensityVal >= onComingThreshold):
        print (" (Vehicle-1 > Vehicle-2 Threshold)")
        print ("    Lowering Vehicle-1 Intensity (5s)")
        print ("\n----------------------------------------------------------") 
        print("                   *")
        print("                 *                               *")
        print("      Vehicle-1=* -->                         <-- *=Vehicle-2")
        print("                 *                               *")
        print("                   *")
        print ("----------------------------------------------------------")
        print("                          *              *")
        print("               Vehicle-1=* -->        <-- *=Vehicle-2")
        print("                          *              *")
        print ("----------------------------------------------------------") 
        print("                    *                           *")
        print("                 <-- *=Vehicle-2     Vehicle-1=* -->")
        print("                    *                           *")
        print ("----------------------------------------------------------")
        setTimer()
        print ("\n Set Initial Vehicle-1 Intensity")
        print ("\n----------------------------------------------------------") 
        print("                                                   *")
        print("                  *                               *")
        print("               <-- *=Vehicle-2         Vehicle-1=* -->")
        print("                  *                               *")
        print("                                                   *")
        print ("----------------------------------------------------------\n") 
        print (" Continue Driving")
    else:    
        print ("\n My Intensity less than Oncoming Vehicle Threshold")
        print ("\n Continue Driving")

def setTimer():
    j = 0
    while j < 5:
        msg6 = " "+str(j)+"s, "
        print (msg6, end="")
        time.sleep(0.5)
        j+=1