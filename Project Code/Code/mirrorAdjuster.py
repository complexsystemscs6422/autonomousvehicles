import time
from random import randint

def mirrorAdjust(init_mirror_angle):

    print("Automatic mirror adjustment system activated...")
    time.sleep(2)
    eyeSensor(init_mirror_angle)

    #optimumAngle()
    #motors(init_mirror_angle, optimum_angle);
    print("Mirror adjustment complete")

def eyeSensor(init_mirror_angle):
    print("Scanner active: searching for user's eyes...")
    time.sleep(2)
    x = randint(-50, 50)
    y = randint(-50, 50)
    z = randint(-50, 50)
    eye_pos = [x,y,z]
    print("Eye position found")
    time.sleep(2)
    optimumAngle(init_mirror_angle, eye_pos)

def optimumAngle(init_mirror_angle, eye_pos):
    print("Calculating optimum angle...")
    time.sleep(2)
    x = abs(eye_pos[0])
    y = abs(eye_pos[1])
    z = abs(eye_pos[2])
    pitch = randint(-x,x)
    yaw  = randint(-y,y)
    roll = randint(-z,z)
    optimum_angle = [pitch, yaw, roll]
    motors(init_mirror_angle, optimum_angle)

def motors(init_mirror_angle, optimum_angle):
    adjustments = []
    print("Calculating adjustments needed in pitch, yaw, and roll")
    for i in range(3):
        a = optimum_angle[i]-init_mirror_angle[i]
        adjustments.append(a)

    pitchAdjustments(adjustments[0])
    yawAdjustments(adjustments[1])
    rollAdjustments(adjustments[2])


def pitchAdjustments(change):
    print("Changing pitch by " + str(change) + " degrees" )
    time.sleep(abs(change)/4)
    return

def yawAdjustments(change):
    print("Changing yaw by " + str(change) + " degrees" )
    time.sleep(abs(change)/4)
    return

def rollAdjustments(change):
    print("Changing roll by " + str(change) + " degrees" )
    time.sleep(abs(change)/4)
    return
